class DtoUsers {
    constructor(obj, usersBooks) {
        this.firstName = obj.firstName
        this.lastName = obj.lastName
        this.username = obj.username
        this.createdAt = obj.createdAt
        this.updatedAt = obj.updatedAt
        this.usersBooks = usersBooks
    }
}

module.exports = DtoUsers