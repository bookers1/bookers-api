﻿const express = require('express');
const router = express.Router();
const authorize = require('../_middleware/authorize')
const usersBooksService = require('./usersBooks.service');

// routes
router.post('/new', authorize(), create);
router.delete('/delete', authorize(), _delete);

module.exports = router;

function create(req, res, next) {
    usersBooksService.create(req.body)
        .then(() => res.json({ message: 'USERS_BOOKS_CREATE' }))
        .catch(next);
}

function _delete(req, res, next) {
    usersBooksService._delete(req.body)
        .then(() => res.json({ message: 'USERS_BOOKS_DELETED' }))
        .catch(next);
}