﻿module.exports = {
    create,
    _delete
};

async function create(params) {
    // save usersBooks
    if (await db.Users_Books.findOne({ where: { user_id: params.user_id, books_url: params.books_url, } })) {
        throw 'ALREADY_ADD';
    }
    await db.Users_Books.create(params);
}

async function _delete(params) {
    const usersBooks = await db.Users_Books.findOne({ where: { user_id: params.user_id, books_url: params.books_url, } })
    await usersBooks.destroy();
}