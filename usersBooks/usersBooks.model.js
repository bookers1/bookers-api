const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        user_id: { type: DataTypes.INTEGER, allowNull: false },
        books_url: { type: DataTypes.STRING, allowNull: false },
    };

    return sequelize.define('Users_Books', attributes);
}