class DtoBooks {
    constructor(obj) {
        this.title = obj.volumeInfo.title
        this.subtitle = obj.volumeInfo.subtitle
        this.authors = obj.volumeInfo.authors
        this.description = obj.volumeInfo.description
        this.covers = obj.volumeInfo.imageLinks
        this.selfLink = obj.selfLink
    }
}

module.exports = DtoBooks