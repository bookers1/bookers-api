﻿const express = require('express');
const router = express.Router();
const authorize = require('../_middleware/authorize')
const bookService = require('./book.service');

// routes
router.get('/:search', authorize(), searchBook);
router.post('/', authorize(), getBook);

module.exports = router;

function searchBook(req, res, next) {
    bookService.searchBook(req.params.search)
        .then(books => res.json(books))
        .catch(next);
}

function getBook(req, res, next) {
    bookService.getBook(req.body)
        .then(book => res.json(book))
        .catch(next);
}