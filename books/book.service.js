﻿const axios = require('axios').default;
const DtoBooks = require('./DtoBooks');

module.exports = {
    searchBook,
    getBook
};

async function searchBook(search) {
    try {
        const response = await axios.get('https://www.googleapis.com/books/v1/volumes?q=' + search + '&maxResults=40');
        return response.data.items.map(book => new DtoBooks(book));
    } catch (error) {
        console.error(error);
    }
}

async function getBook(params) {
    try {
        const response = await axios.get(params.books_url);
        return new DtoBooks(response.data);
    } catch (error) {
        console.error(error);
    }
}
